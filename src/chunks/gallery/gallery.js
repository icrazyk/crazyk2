(function($){

  $('.gallery').each(function(index) {
    var id = $(this).attr('id');
    $(this).find('a').attr('data-fancybox', id);
  });

  // init

  $(".gallery-icon > a").fancybox({
    loop: true,
    caption: function(instance, item) {
      return $(this).closest('.gallery-item').find('figcaption').html();
    }
  });

})(jQuery);
