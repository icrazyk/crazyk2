(function($) {
  var timer, searchStr, jqXhr, keydown,
      $form = $('#search-form'),
      $field = $form.find('.search-form__field'),
      $result = $form.find('.search-result'),
      taxs = ['category', 'tag'],

      // Конфигурация групп поиска

      groups = {
        category: {
          $group: $result.find('.search-result__group_category'),
          $list: $result.find('.search-result__group_category > .search-result__list'),
          data: false,
          rest: {
            endpoint: 'https://crazyk.ru/wp-json/wp/v2/categories',
            config: {
              hide_empty: true,
            },
          },
        },
        tag: {
          $group: $result.find('.search-result__group_tag'),
          $list: $result.find('.search-result__group_tag > .search-result__list'),
          data: false,
          rest: {
            endpoint: 'https://crazyk.ru/wp-json/wp/v2/tags',
            config: {
              hide_empty: true,
            },
          },
        },
        post: {
          $group: $result.find('.search-result__group_post'),
          $list: $result.find('.search-result__group_post > .search-result__list'),
          rest: {
            endpoint: 'https://crazyk.ru/wp-json/wp/v2/posts',
          },
        },
      };

  /*
  * Handlers
  */

  $field.on('keyup', function(e) {
    if(!isArrowKey(e)) searchByClick();
  });

  $field.on('click', function() {
    if(!hasResult()) searchByClick();
  });

  $form.on('click', function(e) { e.stopPropagation(); });

  $result.on('mouseenter', '.search-result__item', mouseSelect);

  $result.on('mouseleave', '.search-result__item', mouseUnselect);

  $(window).on('click', function() {
    clearTimeout(timer);
    hideResultForce();
  });

  $(window).on('keydown', repeatSelectOn);  // Start select and timer for mass select
  $(window).on('keyup', repeatSelectOff);   // Remove timer for mass select

  $(window).on('keypress', enterAjaxResult);

  /*
  * Functions
  */

  /*
  * Выделение результата при наведении курсора
  */

  function mouseSelect() {
    $('.search-result__item').removeClass('search-result__item_selected');
    $(this).addClass('search-result__item_selected');
  }

  /*
  * Снятие выделения рузультата
  */

  function mouseUnselect() {
    $('.search-result__item').removeClass('search-result__item_selected');
  }

  /*
  * Переход к быстрому результату по нажатию Enter
  */

  function enterAjaxResult(e) {
    if((e.keyCode == 13) && $('.search-result__item_selected > a').length) {
      $form.on('submit', function() { return false; });
      location.href = $('.search-result__item_selected > a').attr('href');
    };
  }

  /*
  * Активирует механизм смены выделенного пункта результатов
  * при удержании стрелки вверх / вниз
  */

  function repeatSelectOn(e) {
    if( !(hasResult() && isArrowKey(e)) ) return;

    keydown = true;
    $(window).off('keydown', repeatSelectOn);
    selectByKeyup(e);

    var timer = setInterval(function() {
      if(keydown) {
        selectByKeyup(e);
      } else {
        clearInterval(timer);
      }
    }, 150);
  }

  /*
  * Деактивирует механизм смены выделенного пункта результатов
  * при удержании стрелки вверх / вниз
  */

  function repeatSelectOff(e) {
    if(keydown) {
      keydown = false;
      $(window).on('keydown', repeatSelectOn);
    }
  }

  /*
  * Реализует изменение выделенного пункта в результатах
  * при нажатии стрелки вверх / вниз
  */

  function selectByKeyup(e) {
    var $current, $next, $nextGroup,
        itemClass = 'search-result__item',
        itemSelected = 'search-result__item_selected',
        firstFn,
        nextFn,
        fn = {
          40: {
            first: $().first,
            next: $().next,
          },
          38: {
            first: $().last,
            next: $().prev,
          },
        };

    firstFn = fn[e.keyCode].first;
    nextFn = fn[e.keyCode].next;

    $current = $('.' + itemSelected).length ? $('.' + itemSelected) : firstFn.call($('.' + itemClass));

    if($current.hasClass(itemSelected)) {
      $current.removeClass(itemSelected);
      $next = nextFn.call($current, '.' + itemClass);

      if(!$next.length) {
        $nextGroup = getNextGroup($current.closest('.search-result__group'), nextFn, firstFn);
        $next = firstFn.call($nextGroup.find('.' + itemClass));
      }
    } else {
      $next = $current;
    }

    $next.addClass(itemSelected).find('a');
    scrollToNext($next[0]);
  }

  /*
  * Проскроливание до выреленного пункта, если он вне зоны
  * блока с результатами
  */

  function scrollToNext(next) {
    var nextBottom = next.offsetTop + next.offsetHeight,
        result = $result[0],
        resultScrollBottom = result.scrollTop + result.offsetHeight;

    if(nextBottom > resultScrollBottom) {
      $result.scrollTop(nextBottom - resultScrollBottom + result.scrollTop);
    }

    if(next.offsetTop < result.scrollTop) {
      $result.scrollTop(result.scrollTop - (result.scrollTop - next.offsetTop));
    }
  }

  /*
  * Рекурсивный поиск активной группы в списке результатов
  */

  function getNextGroup($current, nextFn, firstFn) {
    $current = nextFn.call($current);

    if($current.length && $current.hasClass('search-result__group_active')) {
      return $current;
    } else if($current.length) {
      $current = getNextGroup($current, nextFn, firstFn);
    } else {
      $current = firstFn.call($('.search-result__group'));
    }

    return $current;
  }

  /*
  * Поиск с временным интервалом (в ожидании изменения строки поиска)
  */

  function searchByClick(e) {
    updSearchStr();

    if(timer) {
      clearTimeout(timer);
    }

    if(!searchStr) {
      hideResultForce();
      return;
    }

    timer = setTimeout(search, 300);
  }

  /*
  * Главная функция поиска
  */

  function search() {

    // Search from Taxs

    taxs.forEach(function(tax) {
      if(groups[tax].data) {
        searchByTax(tax);
      } else {
        $.get(groups[tax].rest.endpoint,
              groups[tax].rest.config,
              (function(tax) {
                return function(data) {
                  groups[tax].data = data;
                  searchByTax(tax);
                }
              })(tax));
      }
    });

    // Search from Posts

    if(jqXhr) jqXhr.abort();
    jqXhr = $.get(groups.post.rest.endpoint, { search: searchStr }, searchByPosts);
  }

  /*
  * Поиск по таксам (категории / теги)
  */

  function searchByTax(type) {
    var searchRegex,
        result = [];

    searchRegex = new RegExp(searchStr, 'ig');

    result = groups[type].data.filter(function(item) {
      return ~ item.name.search(searchRegex);
    });

    renderResult(result, type);
  }

  /*
  * Поиск по постам
  */

  function searchByPosts(data) {
    var result = data.map(function(item) {
      item.name = item.title.rendered;
      return item;
    });

    renderResult(result, 'post');
  }

  /*
  * Рендер результатов
  */

  function renderResult(result, type) {
    var group = groups[type];

    var resultHTML = result.map(function(item) {
      return '<li class="search-result__item"><a href="' + item.link + '" >' + item.name + '</a></li>';
    }).join('');

    group.$list.html(resultHTML);

    group.$group.toggleClass('search-result__group_active', Boolean(result.length));

    $result.toggleClass('search-result_active', (hasResult()));
    $form.toggleClass('search-form_has-result', (hasResult()));
  }

  function hideResultForce() {
    $result
      .removeClass('search-result_active')
      .find('.search-result__group')
      .removeClass('search-result__group_active')
      .find('search-result__list')
      .empty();

    $form.removeClass('search-form_has-result');
  }

  function updSearchStr() {
    return searchStr = $field.val().trim();
  }

  function hasResult() {
    return Boolean($result.find('.search-result__group_active').length);
  }

  function isArrowKey(e) {
    return Boolean(e.keyCode == 40 || e.keyCode == 38);
  }
})(jQuery);
