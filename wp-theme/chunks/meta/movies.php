<?php

return array(
  'genre' => function($fieldName) {
    return is_array(get_field($fieldName)) ? join(', ', get_field($fieldName)) : get_field($fieldName);
  },
  'frames' => function($fieldName) {
    $imageIds = get_field($fieldName, false, false);
    return do_shortcode('[gallery link="file" ids="' . implode(',', $imageIds) . '"]');
  },
  'trailer' => function($fieldName) {
    return any_video(get_field($fieldName));
  },
  'online' => function($fieldName) {
    return any_video(get_field($fieldName));
  }
);
