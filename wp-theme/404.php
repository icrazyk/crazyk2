<?php
/**
 * The template for displaying 404 pages (not found)
 */

get_header(); ?>
	<section class="entry error-404 not-found">
		<h1 class="entry__title">
      <?php _e( 'Oops! That page can&rsquo;t be found.', 'twentysixteen' ); ?>
    </h1>
		<div class="entry__content">
			<p>
        <?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentysixteen' ); ?>
      </p>
			<?php // get_search_form(); ?>
		</div>
	</section>
<?php get_footer(); ?>
