const fs = require('fs'),
      path = require('path'),
      gulp = require('gulp'),
      exec = require('child_process').exec,
      panini = require('panini'),
      imagemin = require('gulp-imagemin'),
      fontello = require('gulp-fontello'),
      rev = require('gulp-rev'),
      autoprefixer = require('gulp-autoprefixer'),
      connect = require('gulp-connect');

//
// Functions
//

const deleteFolder = (dir) => {
  if (fs.existsSync(dir)) {
    fs.readdirSync(dir).forEach(file => {
      const fullName = path.join(dir, file);

      if (fs.lstatSync(fullName).isDirectory()) {
        deleteFolder(fullName);
      } else {
        fs.unlinkSync(fullName);
      }
    });

    fs.rmdirSync(dir);
  }
};

const deleteFile = (file) => {
  if (fs.existsSync(file) && fs.lstatSync(file).isFile()) {
    fs.unlinkSync(file);
  }
}

//
// Tasks
//

gulp.task('connect', () => {
  connect.server({
    livereload: true
  });
});

gulp.task('clean', (cb) => {
  deleteFolder('./build');
  cb();
});

gulp.task('clean:html', ['build'], (cb) => {
  deleteFile('./build/home.html');
  cb();
});

gulp.task('pages', () => {
  panini.refresh();

  return gulp.src('src/pages/**/*.html')
    .pipe(panini({
      root: 'src/pages/',
      layouts: 'src/layouts/',
      partials: 'src/chunks/',
      helpers: 'src/helpers/',
      data: 'src/data/'
    }))
    .pipe(gulp.dest('dist/'))
    .pipe(connect.reload());
});

gulp.task('icon', () => {
  return gulp.src('./src/chunks/icon/config.json')
    .pipe(fontello())
    .pipe(gulp.dest('./dist/icon/'))
    .pipe(connect.reload());
});

gulp.task('imagemin', ['build'], () => {
  return gulp.src('./build/res/*.{jpg,png}')
    .pipe(imagemin())
    .pipe(gulp.dest('./build/res/'))
});

gulp.task('autoprefixer', ['build'], () => {
  return gulp.src('./build/**/*.css')
    .pipe(autoprefixer())
    .pipe(gulp.dest('./build/'));
});

gulp.task('rev', ['build', 'autoprefixer'], () => {
  return gulp.src('./build/**/*.{js,css}', {base: './build/'})
    .pipe(gulp.dest('./build/'))
    .pipe(rev())
    .pipe(gulp.dest('./build/'))
    .pipe(rev.manifest())
    .pipe(gulp.dest('./build/'))
});

gulp.task('watch', ['icon', 'pages', 'connect'], () => {
  gulp.watch(['./src/{chunks,pages,layouts}/**/*.{html,css,js}'], ['pages']);
  gulp.watch(['./src/chunks/icon/config.json'], ['icon']);
});

gulp.task('build', ['clean', 'icon', 'pages'], (cb) => {
  exec('node ./node_modules/basisjs-tools-build/bin/build build -p -b . -f ./dist/home.html -o ./build/', function (err, stdout, stderr) {
    console.log(stdout);
    console.log(stderr);
    cb(err);
  });
});

gulp.task('default', ['build', 'clean:html', 'imagemin', 'autoprefixer', 'rev']);
